/*
    Proszę zastanowić się w jaki sposób zrealizować:
    - potęgowanie
    - pierwiastkowanie
    - działania trygonometryczne
*/

#include <iostream>

int main()
{
    int l1=0,l2=0;
    char operacja='\0';
    //int a,b;
    //int liczba1,liczba2;
    std::cout << "Program do prostych obliczen\nPodaj pierwsza liczbe: ";
    std::cin >> l1;
    std::cout << "Podaj druga liczbe: ";
    std::cin >> l2;
    std::cout << "Podaj symbol dzialania (+,-,*,/): ";
    std::cin >> operacja;

    // A <= B np. 5 <= 5, 5 <= 6
    // A >= B np. 5 >= 5, 5 >= 7
    // A < B np. 5 < 6
    // A > B np. 4 > 3
    // A == B np. 5==5
    // A != B np. 4!=5
    // "lekcja"=="Lekcja" to nie zadziała bo kod liczbowy litery l i L
    // jest inny! (tablica ASCII)
    if(operacja == '+')
    {
        int c = l1+l2;
        std::cout << "Suma " << l1 << ' '<< operacja << ' ' << l2 <<
                     " = " << c << std::endl;
    }
    else if (operacja=='-')
    {
        std::cout << "Odejmowanie " << l1 << ' '<< operacja << ' ' << l2 <<
                     " = " << l1-l2 << std::endl;
    }
    else if (operacja=='*')
    {
        std::cout << "Mnozenie " << l1 << ' '<< operacja << ' ' << l2 <<
                     " = " << l1*l2 << std::endl;
    }
    else if (operacja=='/')
    {
        if (l2 != 0)
        {
            std::cout << "Dzielenie " << l1 << ' '<< operacja << ' ' << l2 <<
                     " = " << l1/(l2*1.0) << std::endl;
        }
        else
        {
            std::cout << "Przez zero sie nie dzieli!\n";
        }
    }
    else
    {
        std::cout << "Nierozpoznana operacja, koncze program!\n";
    }

    //std::cout << "Podales nastepujace liczby: " << l1 << ' ' << l2 <<
    //             ", chcesz wykonac operacje " << operacja << std::endl;
    return 0;
}
